Exploratory methodological notebooks 

Log-likelihood: See https://nbviewer.jupyter.org/urls/bitbucket.org/dknox_wustl/notebooks/raw/master/spenserverse/ll_comparison.ipynb

Mann-Whitney: See https://nbviewer.jupyter.org/urls/bitbucket.org/dknox_wustl/notebooks/raw/master/spenserverse/Mann-Whitney.ipynb
